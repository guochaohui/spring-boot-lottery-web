package com.lottery.module.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LotteryIndexController {
  @RequestMapping({ "/" })
  public String gotoIndexPage() {
    return "index";
  }
}
