package com.lottery.module.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lottery.module.service.LotterySaleService;
import com.lottery.pojo.BuyInfoDto;
import com.lottery.util.FastJsonUtil;

@Controller
@RequestMapping({ "/buy" })
public class LotterySaleController {

  @Autowired
  private LotterySaleService lotterySaleService;

  @RequestMapping({ "/input" })
  @ResponseBody
  public List<BuyInfoDto> input(String info, String salePlace) {
    List<BuyInfoDto> buyInfoDtos = FastJsonUtil.string2List(info, BuyInfoDto.class);
    buyInfoDtos = this.lotterySaleService.buyNormal(buyInfoDtos, salePlace);
    return buyInfoDtos;
  }

  @RequestMapping({ "/random" })
  @ResponseBody
  public List<BuyInfoDto> random(int count, String salePlace) {
    List<BuyInfoDto> buyInfoDtos = this.lotterySaleService.buyRandom(count, salePlace);
    return buyInfoDtos;
  }
}
