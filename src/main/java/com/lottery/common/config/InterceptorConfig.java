package com.lottery.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.lottery.common.interceptor.AppInterceptor;

@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter {
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(new AppInterceptor()).addPathPatterns(new String[] { "/**" });
    super.addInterceptors(registry);
  }
}
